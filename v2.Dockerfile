ARG TAG=
FROM alpine:${TAG}

# Args
ARG TAG
ARG OVERLAY_VERSION="2.2.0.3"

# Labels
LABEL VERSION="Alpine ${TAG}"

# Environment variables
ENV \
  PS1="$(whoami)@$(hostname):$(pwd)\\$ " \
  HOME="/root" \
  TERM="xterm"

RUN \
  echo "**** Building Alpine ${TAG} ****" && \
  echo "**** Upgrade alpine base ****" && \
  apk add --no-cache -U ca-certificates && \
  apk upgrade --no-cache -U && \
  echo "**** Install build packages ****" && \
  apk add --no-cache -U --virtual=build-dependencies \
    tar && \
  echo "**** Install runtime packages ****" && \
  apk add --no-cache -U \
    bash \
    coreutils \
    curl \
    shadow \
    tzdata \
    xz && \
  echo "**** Add s6 overlay ****" && \
  # Find arch for archive
  ARCH=$(uname -m) && \
  OVERLAY_ARCH="" && \
  [ "${ARCH}" = "x86_64" ] && OVERLAY_ARCH="amd64" || true && \
  [ "${ARCH}" = "aarch64" ] && OVERLAY_ARCH="aarch64" || true && \
  [ "${ARCH}" = "armv7l" ] && OVERLAY_ARCH="armhf" || true && \
  curl -fsS -o /tmp/s6-overlay.tar.gz -L \
    "https://github.com/just-containers/s6-overlay/releases/download/v${OVERLAY_VERSION}/s6-overlay-${OVERLAY_ARCH}.tar.gz" && \
  tar xfz \
    /tmp/s6-overlay.tar.gz -C / && \
  # v3 compatibility
  ln -s /usr/bin /command && \
  echo "**** Create abc user and make our folders ****" && \
  groupmod -g 1000 users && \
  groupadd -g 911 abc && \
  useradd -u 911 -g abc -G users -d /config -s /bin/false abc && \
  mkdir -p \
    /app \
    /config \
    /defaults && \
  echo "**** Cleanup ****" && \
  apk del --purge \
    build-dependencies && \
  rm -rf \
    /tmp/*

# Add extra files
COPY root/ /

ENTRYPOINT ["/init"]
