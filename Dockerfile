ARG TAG=
FROM alpine:${TAG}

# Args
ARG TAG
ARG OVERLAY_VERSION="3.2.0.2"

# Labels
LABEL VERSION="Alpine ${TAG}"

# Environment variables
ENV \
  PS1="$(whoami)@$(hostname):$(pwd)\\$ " \
  HOME="/root" \
  TERM="xterm"

RUN \
  echo "**** Building Alpine ${TAG} ****" && \
  echo "**** Upgrade alpine base ****" && \
  apk add --no-cache -U ca-certificates && \
  apk upgrade --no-cache -U && \
  echo "**** Install build packages ****" && \
  apk add --no-cache -U --virtual=build-dependencies \
    tar && \
  echo "**** Install runtime packages ****" && \
  apk add --no-cache -U \
    bash \
    coreutils \
    curl \
    shadow \
    tzdata \
    xz && \
  echo "**** Add s6 overlay ****" && \
  # Find arch for archive
  ARCH=$(uname -m) && \
  OVERLAY_ARCH="" && \
  [ "${ARCH}" = "x86_64" ] && OVERLAY_ARCH="x86_64" || true && \
  [ "${ARCH}" = "aarch64" ] && OVERLAY_ARCH="aarch64" || true && \
  [ "${ARCH}" = "armv7l" ] && OVERLAY_ARCH="armhf" || true && \
  # S6 scripts
  curl -fsS -O --output-dir /tmp/ -L \
    "https://github.com/just-containers/s6-overlay/releases/download/v${OVERLAY_VERSION}/s6-overlay-noarch.tar.xz" && \
  tar -C / -Jxpf "/tmp/s6-overlay-noarch.tar.xz" && \
  # S6 binary
  curl -fsS -O --output-dir /tmp/ -L \
    "https://github.com/just-containers/s6-overlay/releases/download/v${OVERLAY_VERSION}/s6-overlay-${OVERLAY_ARCH}.tar.xz" && \
  tar -C / -Jxpf "/tmp/s6-overlay-${OVERLAY_ARCH}.tar.xz" && \
  # S6 symlinks
  curl -fsS -O --output-dir /tmp/ -L \
    "https://github.com/just-containers/s6-overlay/releases/download/v${OVERLAY_VERSION}/s6-overlay-symlinks-noarch.tar.xz" && \
  tar -C / -Jxpf "/tmp/s6-overlay-symlinks-noarch.tar.xz" && \
  curl -fsS -O --output-dir /tmp/ -L \
    "https://github.com/just-containers/s6-overlay/releases/download/v${OVERLAY_VERSION}/s6-overlay-symlinks-arch.tar.xz" && \
  tar -C / -Jxpf "/tmp/s6-overlay-symlinks-arch.tar.xz" && \
  echo "**** Create abc user and make our folders ****" && \
  groupmod -g 1000 users && \
  groupadd -g 911 abc && \
  useradd -u 911 -g abc -G users -d /config -s /bin/false abc && \
  mkdir -p \
    /app \
    /config \
    /defaults && \
  echo "**** Cleanup ****" && \
  apk del --purge \
    build-dependencies && \
  rm -rf \
    /tmp/*

# Add extra files
COPY root/ /

ENTRYPOINT ["/init"]
